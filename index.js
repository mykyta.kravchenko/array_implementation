function MyArray() {
  for(let i = 0; i < arguments.length; i++) {
    this[i] = arguments[i];
  }

  this.length = arguments.length;

  
}

MyArray.prototype[Symbol.iterator] = function () {
  let current = 0;
  let last = this.length;
  return {
    next: () => {
      if(current < last) {
        return {
          done: false,
          value: this[current++]
        };
      }
      
      return { done: true };
    }
  }
};

MyArray.prototype.push = function (item) {
  this[this.length] = item;
  this.length++;
  return this.length;
};

MyArray.prototype.pop = function () {
  let item = this[this.length - 1];
  delete this[this.length - 1];
  this.length--;
  return item;
};

MyArray.prototype.from = function (arg) {
  if(typeof arg[Symbol.iterator] === "function") {
    let arr = new MyArray();
    for( item of arg ) {
      arr.push(item);
    }
    return arr;
  }
}

MyArray.prototype.map = function (callback) {
  let arr = new MyArray();
  let thisArg = this;
  if(arguments[1]) thisArg = arguments[1];
  for(let i = 0; i < thisArg.length; i++) {
    let item = callback(thisArg[i], i, thisArg);
    arr.push(item);
  }
  
  return arr;
}

MyArray.prototype.forEach = function(callback) {
  let thisArg = this;
  if(arguments[1]) thisArg = arguments[1];
  for(let i = 0; i < thisArg.length; i++) {
    callback(thisArg[i], i, thisArg);
  }
}

MyArray.prototype.reduce = function(callback) {
  let thisArg = this;
  let result = 0;
  if(arguments[1]) result = arguments[1];
  for(let i = 0; i < thisArg.length; i++) {
    result = callback(result, thisArg[i], i, thisArg);
  }
  return result;
}

MyArray.prototype.filter = function(callback) {
  let arr = new MyArray();
  let thisArg = this;
  if(arguments[1]) thisArg = arguments[1];
  for(let i = 0; i < thisArg.length; i++) {
    if(callback(thisArg[i], i, thisArg)) arr.push(thisArg[i]);
  }
  return arr;
}

MyArray.prototype.toString = function() {
  let string = '';
  let thisArg = this;
  for(let i = 0; i < thisArg.length; i++) {
    string += thisArg[i];
    if(i !== thisArg.length - 1) string += ',';
  }
  return string;
}

MyArray.prototype.sort = function() {
  let thisArg = this;
  let compareFunction = function (a, b) {
    if(a.toString() > b.toString()) return 1;
  };
  if(arguments[0]) compareFunction = arguments[0];
  for(let i = 0; i < thisArg.length; i++) {
    for(let j = i + 1; j < thisArg.length; j++) {

      if(compareFunction(thisArg[i], thisArg[j]) > 0) {
        let temp = thisArg[i];
        thisArg[i] = thisArg[j];
        thisArg[j] = temp;
      } 

    }
  }
  return thisArg;
}